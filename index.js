// Function thêm số
var numArr = [];
document.getElementById("btn-themso").addEventListener("click",function(){
    var inputNumValue = document.getElementById("txt-number").value*1;

    numArr.push(inputNumValue);

    document.getElementById("txt-number").value = null;

    document.getElementById("show_arr").innerHTML = numArr;


    // Bài 1: function tính tổng số dương trong mảng
    document.getElementById("btn-tong-so-duong").addEventListener("click",function(){
        var sumPositiveNum = 0;

        for(index = 0; index < numArr.length; index++){
            if(numArr[index] > 0){
                sumPositiveNum += numArr[index];
            }
        }
        document.getElementById("show-tong-so-duong").innerHTML = `Tổng giá trị số dương trong mảng: ${sumPositiveNum}`;
    })

    // Bài 2: function đếm số dương
    document.getElementById("btn-dem-so-duong").addEventListener("click",function(){
        var countSoDuong = 0;

        for(index = 0; index < numArr.length; index++){
            if(numArr[index] > 0){
                countSoDuong++;
            }
        }
        document.getElementById("show-dem-so-duong").innerHTML = `Tổng số dương trong mảng: ${countSoDuong}`;
    })

    // Bài 3: function tìm số nhỏ nhất trong mảng
    document.getElementById("btn-num-min").addEventListener("click",function(){
        var numMin = numArr[0];
        for(index = 1; index < numArr.length; index++){
            if(numMin > numArr[index]){
                numMin = numArr[index];
            }
        }
        document.getElementById("show-num-min").innerHTML = `Giá trị nhỏ nhất trong mảng: ${numMin}`;
    })

    // Bài 4: function tìm số dương nhỏ nhất
    document.getElementById("btn-duong-min").addEventListener("click",function(){
        var numDuongArr = [];

        // Lấy ra số dương trong mảng và đưa vào mảng mới
        for(index = 0;index < numArr.length; index++){
            if(numArr[index] > 0){
                numDuongArr.push(numArr[index])
            }
        }

        var numDuongMin = numDuongArr[0];
        // Lấy ra số nhỏ nhất trong mảng mới
        if(numDuongArr.length == 0){
            document.getElementById("show-duong-min").innerHTML = "Mảng không chứa số dương nào";
        }
        else{
            for(i = 1; i< numDuongArr.length; i++){
                if(numDuongArr[i] < numDuongMin){
                    numDuongMin = numDuongArr[i];
                }
            }
            document.getElementById("show-duong-min").innerHTML = `Giá trị dương nhỏ nhất trong mảng: ${numDuongMin}`;
        }
    })

    // Bài 5: function tìm số chẵn cuối cùng
    document.getElementById("btn-chan-end").addEventListener("click",function(){
        var numChan = [];

        for(index = 0;index < numArr.length; index++){
            if(numArr[index]%2 == 0){
                numChan.push(numArr[index]);
            }
        }

        var numChanEnd = null;
        if(numChan.length == 0){
            document.getElementById("show-chan-end").innerHTML = "Không tồn tại số chẵn trong mảng";
        }
        else{
            numChanEnd = numChan[(numChan.length-1)];
            document.getElementById("show-chan-end").innerHTML = `Số chẵn cuối cùng trong mảng: ${numChanEnd} `;
        }
    })

    // Bài 6: function đổi chỗ
    document.getElementById("btn-doi-cho").addEventListener("click",function(){
        var index1 = document.getElementById("txt-index1").value*1;
        var index2 = document.getElementById("txt-index2").value*1;
        
        var numTam = null;

        if(index1 >= numArr.length || index2 >= numArr.length){
            document.getElementById("show-doi-cho").innerHTML = `Số nhập vào không >= ${numArr.length}`;
        }
        else{
            numTam = numArr[index1];

            numArr[index1] = numArr[index2];
            numArr[index2] = numTam;

            document.getElementById("show-doi-cho").innerHTML = `Mảng sau khi đổi: ${numArr}`;
        }
    })

    // Bài 7: function sắp xếp tăng dần
    document.getElementById("btn-sort").addEventListener("click",function(){
        var tam = null;
        for(i = 0; i < numArr.length; i++){
            for(j = i+1; j < numArr.length; j++){
                if(numArr[j]<numArr[i]){
                    tam = numArr[i];
                    numArr[i] = numArr[j];
                    numArr[j] = tam;
                }
            }
        }
        document.getElementById("show-sort").innerHTML = `Mảng sau khi sắp xếp: ${numArr}`;
    })

    // Bài 8: function tìm số nguyên tố đầu tiên
    document.getElementById("btn-snt").addEventListener("click",function(){
        var countNumArr = [];
        var sntArr = [];
        for(index = 0; index < numArr.length; index++){
            if(numArr[index] > 1){
                countNumArr.push(numArr[index]);
            }
        }
        if(countNumArr.length == 0){
            document.getElementById("show-snt").innerHTML = -1;
        }
        else{
            for(i = 0; i < countNumArr.length; i++){
                var count = 0;
                for(j = 2; j <= Math.sqrt(countNumArr[i]); j++){
                    if(countNumArr[i]%j == 0){
                        count++;
                    }
                }
                if(count == 0){
                    sntArr.push(countNumArr[i]);
                }
            }
            document.getElementById("show-snt").innerHTML = sntArr[0];
        }
    })
})

// Bài 9: Đếm số nguyên
var newArr = [];
document.getElementById("btn-themso-newArr").addEventListener("click",function(){
    var inputNewNumValue = document.getElementById("txt-numberNewArr").value*1;

    newArr.push(inputNewNumValue);

    document.getElementById("txt-numberNewArr").value = null;
    document.getElementById("show_newArr").innerHTML = newArr;

    document.getElementById("btn-dem-so-nguyen").addEventListener("click",function(){
        var count = 0;

        for(index = 0;index < newArr.length;index++){
            if(Number.isInteger(newArr[index]) == true){
                count++;
            }
        }
        document.getElementById("show-dem-so-nguyen").innerHTML = `Số nguyên: ${count} `;
    })

    // Bài 10: function so sánh số lượng số nguyên âm và dương
    document.getElementById("btn-am-duong").addEventListener("click",function(){
        var countAm = 0;
        var countDuong = 0;
        var soSanh = "";

        // Số 0 là số trung tính
        for(index = 0;index < newArr.length;index++){
            if(newArr[index]<0){
                countAm++;
            }
            else{
                if(newArr[index]>0)
                countDuong++;
            }
        }

        if(countAm == countDuong){
            soSanh = "=";
        }
        else if(countAm > countDuong){
            soSanh = ">";
        }
        else{
            soSanh = "<";
        }
        document.getElementById("show-am-duong").innerHTML = `Trong mảng có ${countAm} số âm và ${countDuong} số dương, số âm ${soSanh} số dương`;
    })
})


